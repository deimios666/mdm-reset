function dostuff(e) {
  (async () => {
    const username = document.querySelector("#username").value;
    const password = document.querySelector("#password").value;
    const imei = document.querySelector("#imei").value;
    const messageElem = document.querySelector("#message");

    const params = `username=${encodeURI(username)}&password=${encodeURI(password)}&imei=${encodeURI(imei)}`;

    const response = await fetch("dostuff.php", {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Content-type": "application/x-www-form-urlencoded"
      },
      body: params
    });

    const responseText = await response.text();

    if (responseText === "Reset done") {
      messageElem.classList.remove("alert-danger");
      messageElem.classList.add("alert-success");
      messageElem.innerHTML = `Comanda de resetare a fost trimisă cu succes pentru tableta cu IMEI ${imei}`;

    } else {
      messageElem.classList.remove("alert-success");
      messageElem.classList.add("alert-danger");
      messageElem.innerHTML = `Error: ${responseText}\n`;
    }
  })();
  e.preventDefault();
}
