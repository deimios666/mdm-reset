<?php

//====Orange user/password
$isjusername = "ISJ_Covasna";
$isjpassword = "parola orange";
$checkDB = true; //if you want to check imei in the database (db.txt)
//=========================

$isjmd5password = strtoupper(md5($isjpassword));
$username = $_POST["username"];
$password = $_POST["password"];
$imei = $_POST["imei"];

$logline = date('Y-m-d H:i:s') . "\t$username\t$imei\t";
$status = "";
$schoolname = "";

function doLog($line)
{
  $file = "log.txt";
  if (is_writable($file)) {
    $fhandle = fopen($file, "a");
    fwrite($fhandle, $line);
    fclose($fhandle);
  }
}

//URL CONFIG
$mainUrl = "https://www.siiir.edu.ro/siiir/";
$sessionInitUrl = $mainUrl . "login.jsp"; //to get session cookie
$authUrl = $mainUrl . "j_spring_security_check"; //to login

$conn = curl_init();
//=======URL 1
curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($conn, CURLOPT_SSLVERSION, 6);
//curl_setopt($conn, CURL_SSLVERSION_TLSv1);
curl_setopt($conn, CURLOPT_COOKIESESSION, true);
curl_setopt($conn, CURLOPT_URL, $sessionInitUrl);
curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
curl_setopt($conn, CURLOPT_COOKIEFILE, "cookies.txt");
curl_setopt($conn, CURLOPT_COOKIEJAR, "cookies.txt");
curl_setopt($conn, CURLOPT_SSL_VERIFYPEER, 0);
curl_exec($conn);

//=======URL 2
$fields = array(
  'j_username' => urlencode($username),
  'j_password' => urlencode($password)
);
$fields_string = "";
foreach ($fields as $key => $value) {
  $fields_string .= $key . '=' . $value . '&';
}
rtrim($fields_string, '&');
curl_setopt($conn, CURLOPT_POST, count($fields));
curl_setopt($conn, CURLOPT_POSTFIELDS, $fields_string);
curl_setopt($conn, CURLOPT_URL, $authUrl);
//curl_setopt($conn, CURLOPT_RETURNTRANSFER, true);
curl_setopt($conn, CURLOPT_HTTPHEADER, array(
  "Accept: application/json",
  "Accept-Charset: UTF-8",
  "charset: UTF-8",
  "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
  "X-Requested-With: XMLHttpRequest"
));
$authResult = curl_exec($conn);

if ($authResult == "{success:true}") {
  //We got SIIIR login
  //get school from SIIIR
  $dataUrl = $mainUrl . "home/AMS/util/AppConfig.js?_dc=" . preg_replace('/\D/', '', microtime());
  curl_setopt($conn, CURLOPT_URL, $dataUrl);
  curl_setopt($conn, CURLOPT_HTTPGET, "");
  $siiirappconfig = curl_exec($conn);

  //'entity':{"id":11312330,"code":"1461100404","internalId":11003329,"shortName":"LIC. TEOR. \"MIKES KELEMEN\" SFÂNTU GHEORGHE"}
  $siiirRegex = "/'entity':{([^}]+)}/m";
  preg_match($siiirRegex, $siiirappconfig, $matches);
  $siiirResultJSON = json_decode("{" . $matches[1] . "}");
  $schoolname .= $siiirResultJSON->shortName . "\t";
  //get captured group from regex, split by comma,
  //get second element, split that by colon, 
  //get second element, strip off first and last char
  $siiirCode = substr(explode(":", explode(",", $matches[1])[1])[1], 1, -1);

  //check school vs imei list
  $db = file("db.txt", FILE_IGNORE_NEW_LINES);
  $pattern = $imei . "\t" . $siiirCode;

  $proceed = true;
  if ($checkDB) {
    if (array_search($pattern, $db, TRUE) === FALSE) {
      $status .= "IMEI $imei doesn't belong to siiircode $siiirCode";
      $proceed = false;
    }
  }

  if ($proceed) {
    //if allowed, connect and reset

    $MDMloginUrl = "http://mobidev.rpss.ro/rest/public/auth/login";

    $payload = json_encode(array(
      "login" => $isjusername,
      "password" => $isjmd5password
    ));

    $conn2 = curl_init();
    //STEP1 - Login
    curl_setopt($conn2, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($conn2, CURLOPT_SSLVERSION, 6);
    curl_setopt($conn2, CURLOPT_COOKIESESSION, true);
    curl_setopt($conn2, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($conn2, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($conn2, CURLOPT_HEADER, 1);
    curl_setopt($conn2, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($conn2, CURLOPT_POSTFIELDS, $payload);
    curl_setopt($conn2, CURLOPT_URL, $MDMloginUrl);
    $MDMloginResult = curl_exec($conn2);

    $header_size = curl_getinfo($conn2, CURLINFO_HEADER_SIZE);
    $header = substr($MDMloginResult, 0, $header_size);
    $MDMloginResultBody = substr($MDMloginResult, $header_size);

    $MDMuser = json_decode($MDMloginResultBody)->data;

    $MDMuserString = json_encode($MDMuser);

    $headerLines = explode("\r\n", $header);

    foreach ($headerLines as $line) {
      if (substr($line, 0, 12) === "Set-Cookie: ") {
        $cookie = substr($line, 12);
        break;
      }
    }

    $cookieArray = explode("; ", $cookie);
    $cookie = $cookieArray[0] . "; user=" . urlencode($MDMuserString);

    $requestHeaders = array(
      'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:83.0) Gecko/20100101 Firefox/83.0',
      'Accept: application/json, text/plain, */*',
      'Accept-Language: en-US,en;q=0.5',
      'Accept-Encoding: gzip, deflate',
      'Referer: http://mobidev.rpss.ro/',
      'If-Modified-Since: Thu, 01 Jan 1970 00:00:00 GMT',
      'DNT: 1'
    );

    //STEP2 - Get deviceID
    $MDMstatusUrl = "http://mobidev.rpss.ro/rest/plugins/devicereset/private/status/$imei";
    curl_setopt($conn2, CURLOPT_COOKIE, $cookie);
    curl_setopt($conn2, CURLOPT_HTTPGET, 1);
    curl_setopt($conn2, CURLOPT_HEADER, 0);
    curl_setopt($conn2, CURLOPT_HTTPHEADER, $requestHeaders);
    curl_setopt($conn2, CURLOPT_URL, $MDMstatusUrl);

    $MDMstatusResult = curl_exec($conn2);

    $MDMStatus = json_decode($MDMstatusResult)->data;

    $deviceId = $MDMStatus->deviceId;


    //STEP3 - Reset
    $MDMresetUrl = "http://mobidev.rpss.ro/rest/plugins/devicereset/private/" . $deviceId;
    curl_setopt($conn2, CURLOPT_PUT, 1);
    curl_setopt($conn2, CURLOPT_COOKIE, $cookie);
    curl_setopt($conn2, CURLOPT_URL, $MDMresetUrl);

    $MDMresetResult = curl_exec($conn2);

    $MDMresetResultObj = json_decode($MDMresetResult);

    if ($MDMresetResultObj->status === "OK") {
      $status .= "Reset done";
    } else {
      $status .= "Error resetting: $MDMresetResult";
    }
  }
} else {
  $status .= "Invalid SIIIR username or password or failed to connect to SIIIR";
}
echo $status;
doLog($logline . $schoolname . $status . "\n");
